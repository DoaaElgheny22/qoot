export const environment = {
  production: true,
  defaultDateFormat: 'DD-MM-YYYY',
  defaultPageSize: 20,
  api_imges: 'https://backend-qoot.qoot.online/',
  api_url: 'https://backend-qoot.qoot.online/api',
  defaultLanguage: 'en',
};
